# Variables para identificar el proyecto en Google Cloud Platform
variable "project_id" {
  description = "The name for the Project ID"
  default     = "my-firt-project-347213"
}

# Variables para identificar el cluster de GKE
variable "cluster_name" {
  description = "The name for the GKE cluster"
  default     = "demo-jenkins-cluster"
}

# Variables para identificar el entorno del cluster
variable "env_name" {
  description = "The environment for the GKE cluster"
  default     = "dev"
}

# Variables para identificar la región en la que se alojará el cluster
variable "region" {
  description = "The region to host the cluster in"
  default     = "us-central1"
}

# Variables para identificar la red virtual privada (VPC) que alojará el cluster
variable "network" {
  description = "The VPC network created to host the cluster in"
  default     = "gke-network"
}

# Variables para identificar la subred que alojará el cluste
variable "subnetwork" {
  description = "The subnetwork created to host the cluster in"
  default     = "gke-subnet"
}

# Variables para identificar el rango IP secundario para usar para los pods
variable "ip_range_pods_name" {
  description = "The secondary ip range to use for pods"
  default     = "ip-range-pods"
}

# Variables para identificar el rango IP secundario para usar para los servicios
variable "ip_range_services_name" {
  description = "The secondary ip range to use for services"
  default     = "ip-range-services"
}
